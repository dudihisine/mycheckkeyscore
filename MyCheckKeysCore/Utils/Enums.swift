//
//  Enums.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 28/05/2020.
//  Copyright © 2020 MyCheck. All rights reserved.
//

import Foundation


public enum MKBTPermissionStatus: Int{
    case notDetermined
    case restricted
    case denied
    case allowedAlways
    case unknown
}
