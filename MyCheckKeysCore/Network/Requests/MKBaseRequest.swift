//
//  MKBaseRequest.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 17/06/2019.
//  Copyright © 2019 ErezSpatz. All rights reserved.
//

import UIKit

class MKBaseRequest: NSObject {

    //implement in sub class
    func getModuleName()->String{
        return ""
    }
    
    //implement in sub class
    func getJsonModuleName()->String{
        return ""
    }
    
    //implement in sub class if needed
    func getBody()->Dictionary<String , Any>?{
        return nil
    }
    
    func getHeader()->Dictionary<String , String>?{
        return nil
    }
    
    //implement in sub class
    func parseData(from response : [String : Any]){
        
    }
    
    //implement in sub class
    func notifyRequestFinished(with error : Error){
        
    }
    
    //implement in sub class
    func notifyRequestFinishedSuccessfully(){
        
    }
    
}
