//
//  ViewController.swift
//  CoreTest
//
//  Created by Dudi Hisine on 27/05/2020.
//  Copyright © 2020 MyCheck. All rights reserved.
//

import UIKit
import MyCheckKeysCore
import MyCheckUtils

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let deviceID = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteWNoZWNrYXBwLmNvbSIsImF1ZCI6IlRFU1QiLCJzdWIiOnsiaWQiOjMzMDAzLCJ1c2VyX2V4dGVybmFsX2lkIjoiMzRkMjM2MzEtMmIxNC00MTA4LTlmMjEtMzgxMGNmMjQ3NWY5IiwiYnVzaW5lc3NfaWQiOjE5NCwiYnVzaW5lc3NfY2hpbGRyZW5faWRzIjpbXSwidHlwZSI6InVzZXIiLCJwZXJtaXNzaW9ucyI6WyJVU0VSUy5XUklURS5TRUxGLlVTRVIiLCJXQUxMRVQuV1JJVEUuVVNFUi5QQVlNRU5UX01FVEhPRCIsIlVTRVJTLlJFQUQuQlVTSU5FU1MuQlVTSU5FU1MiLCJHSUZUQ0FSRFMuV1JJVEUuVVNFUi5QQVlNRU5UTUVUSE9EIiwiUEFZTUVOVC1SRVFVRVNULU1BTkFHRVIuUkVBRC5VU0VSLlBBWU1FTlQtUkVRVUVTVCIsIldBTExFVC5XUklURS5VU0VSLkFMVEVSTkFUSVZFX1BBWU1FTlQiLCJIVUIuUkVBRC5CVVNJTkVTUy5SRVNUQVVSQU5UIl0sInJvbGVzIjpbInVzZXIiXX0sImV4cCI6MTU5MDE3NTkxNSwiaXNzdWVkX2F0IjoxNTkwMTc1OTE1LCJrZXkiOiIyNExqYWlTU2NNMzJnTXFPIn0.CM9n7yNbiVxBhUInUQ3X4OYYJj5aW3puMeMwLhZw5S8"
        let pk = "pk_zEfJ9IlLWuF9zxl5sTbUElW4IbfJ1"
        let rt = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteWNoZWNrYXBwLmNvbSIsImF1ZCI6IlRFU1QiLCJzdWIiOnsidHlwZSI6InJlZnJlc2hUb2tlbiIsImVuY190b2tlbiI6ImV5SnBkaUk2SWpGYVdESTNWRlZZVWpKbVMzRkVRa1JZZUcxSk9FRTlQU0lzSW5aaGJIVmxJam9pWTBGQlQweERWMWxXTTNkWk5GcFJSa2swYWtoclltMWxaVUZUY1VJeVVVOXJabWRqV25kcU5EWllTbFpHYlhFck9FdGpOVWxLWW5Rd1VVUk9WWEkyTVdWYVZVd3JVVk00U0dwalIxUklPRTloTUVKRWR6RnZiWFJXYzJnNGQyZFVZbXRVWnpabk5uTnZlVE01ZUU5T05WQkZOalJMVVVjeFZFeFVjbU5jTDJ4NWVHbDZSVnd2Y0U4MVZqYzRhekpIY1Z3dlIzZEJYQzk1UW1kRFNqTk9WREV3VFZoaWFVZHlabXRVZDJkUlZISjBjbVZSU2sweFVuUldVRlk0ZFdaNVZGbDZVa3B6V0d4dU5sRnJNbHd2U21zeVozSXdObG81ZFVFeGRVcGhZVlZ2TTJ4dlJGcHllVTF6YmpGaU1IcHFUMEkwYm5rd1dHRjJSVTVMU0dVelVtdGhiSGRtY1ZSdVVFTlhXazVLYW1GWVVGQXdWRFE1YUVkdFJVRjZSREJaTVVWbVF5c3lWRzVPVkdsV1pHVTVjVzB3U25sYVFqRklTa3hYTjNrd2FHUnFXVVJ2WlZSU1duSmNMMDFFV0dGemFGZFBORGN4TlV4SVZYRk1VSGQ1Um5GY0wyUkhVbVJ5Vnl0a1ZuVkZjbk5XZGxCRE0ybFBORGxxWjJoc2FsbExVSEpZU3pOeU1WYzBObWxtU0VOcVVtbEJOR2tyYmpkamJrODJUMkZOTTJGalFrczBaU3R5UkRkelpXcEpVa014Y0hjOUlpd2liV0ZqSWpvaU5HRTVZamhqTmpGak1XWmhNV1ppTVRnM1lqZGhaV0V6TjJGbFpqaG1OekF3WW1WbU0yTmhaVEptTTJaa01tSmxaRFk1TURVMVkyVTJZakJsWXpSall5SjkifSwiZXhwIjoxNTkyNzMxNzgzLCJpc3N1ZWRfYXQiOjE1OTI3MzE3ODMsImtleSI6IjN2bXkwd1VhRkpKS09LUnEifQ.3cD2yarrBiLLunnHoS4FzDreENJySZHSvlWQz5_dhE0"
        let at = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJteWNoZWNrYXBwLmNvbSIsImF1ZCI6IlRFU1QiLCJzdWIiOnsiaWQiOjMxODk1LCJ1c2VyX2V4dGVybmFsX2lkIjoiZXJlZmR4YyIsImJ1c2luZXNzX2lkIjoyLCJidXNpbmVzc19jaGlsZHJlbl9pZHMiOltdLCJ0eXBlIjoidXNlciIsInBlcm1pc3Npb25zIjpbIlVTRVJTLldSSVRFLlNFTEYuVVNFUiIsIldBTExFVC5XUklURS5VU0VSLlBBWU1FTlRfTUVUSE9EIiwiVVNFUlMuUkVBRC5CVVNJTkVTUy5CVVNJTkVTUyIsIkdJRlRDQVJEUy5XUklURS5VU0VSLlBBWU1FTlRNRVRIT0QiLCJQQVlNRU5ULVJFUVVFU1QtTUFOQUdFUi5SRUFELlVTRVIuUEFZTUVOVC1SRVFVRVNUIiwiV0FMTEVULldSSVRFLlVTRVIuQUxURVJOQVRJVkVfUEFZTUVOVCIsIkhVQi5SRUFELkJVU0lORVNTLlJFU1RBVVJBTlQiXSwicm9sZXMiOlsidXNlciJdfSwiaWF0IjoxNTcwMTA5ODg4LCJleHAiOjE1NzAxNDU4ODgsImtleSI6InhHVVlQWVJBbVNISHRSNVcifQ.gvYgrSWc1NB3M-STm3-FgR4ZtyqOc8KkxRlsowzua8s"

        let config = MKConfig(environment: .test, publishableKey: pk, refreshToken: rt, accessToken: at, deviceID: deviceID, debugMode: true)

        let provider = AbstructMyCheckKeys()

        provider.config(config: config, and: self)

        provider.sendDevicesRequest(delegate: self)
    }

}

extension ViewController: MyCheckKeysDelegate{
    func onInit() {

    }

    func onFailure(error: NSError) {
        print(error)
    }

    func onAvailableRoomsReceived(rooms: [MKRoom]) {

    }

    func onDoorUnlocked() {

    }

    func onDoorUnlockedFailed(message: String) {

    }

    func onDeactivateAllKeys() {

    }
}

extension ViewController: MKDevicesRequestDelegate{
    func didFinishRequestWithData(data: MKDevicesData?) {
        print("token: \(String(describing: data?.token)), externalDeviceID: \(String(describing: data?.externalDeviceId))")
    }

    func didFinishWithError(error: Error) {
        print(error)
    }
}

