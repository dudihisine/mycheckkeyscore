# MyCheck Keys Core SDK

An Core SDK for MyCheck Hotels SDKs.


## Requirements

iOS 11 or above.

Swift 5.0

## Installation

MyCheck Keys SDK is available through [CocoaPods](http://cocoapods.org). install it by simply adding the following lines to your Podfile Inside the target:

```
pod "MyCheckKeysCore"
```
Now you can run 'pod install'

### additional config

#### Info.plist
Add this Key to your 'info.plist'

Privacy - Bluetooth Peripheral Usage Description
Privacy - Bluetooth Always Usage Description

#### Capabilities
Add Background Modes to you App Capabilities.

Make sure this value is selected:

Acts as a bluetooth LE accessory

## Use
The MKCore Singleton will be the point of contact that will allow you to login and get the basic data for other Hotel SDKs. 

Before using any other functions you will have to login the user. Login is done by first obtaining a refresh token from your server (that, in turn, will obtain it from the MyCheck server using the secret).
Once you are logged in, you can open a table. Now you can start doing  dine in specific actions using the Dine singleton. Import the Dine module of the SDK.

```
import MyCheckKeysCore
```

### AbstructMyCheckKeys - abstruct class
This class is used as base for other SDKs.

### MKConfig
This class represent the config data.
Create config instance by call:

```
let config = MKConfig(environment: env, publishableKey: pk, refreshToken: rt, accessToken: at, deviceID: deviceID, debugMode: true)

```
the environment and debugMode parmas has default values of:
environment = .prod
debugMode = false

### MKRoom
This class is a representation of the room object.
It holds on the room data like index, room number etc.

## Authors

Dudi hisine, dudih@mycheck.co.il
## License

Please read the LICENSE file available in the project

