//
//  MKNetworkManager.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 17/06/2019.
//  Copyright © 2019 ErezSpatz. All rights reserved.
//

import UIKit
import MyCheckUtils

class MKNetworkManager: NSObject {

    private let environment: MCUEnvironment!
    
    private var baseURL : String {
        get{
            switch environment {
            case .test:
                return "https://the-test.mycheckapp.com/virtual-room-keys/api/v1"
            case .sandbox:
                return "https://the-sandbox.mycheckapp.com/virtual-room-keys/api/v1"
            default:
                //prod
                return "https://the.mycheckapp.com/virtual-room-keys/api/v1"
            }
        }
    }
    
    var baseHeaders : [String : String] = [
        "Content-Type": "application/json"
    ]

    init(environment: MCUEnvironment) {
        self.environment = environment
        super.init()
    }
    
    func sendRequest(with requestProvider : MKBaseRequest){
        guard let url = URL(string:"\(baseURL)/\(requestProvider.getModuleName())") else{
            return
        }
        
        let session = URLSession.shared
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        request.httpMethod = "POST"
        
        //set Headers
        
        if let header = requestProvider.getHeader(){
            baseHeaders = mergeHeaders(dicts: [baseHeaders,header])
        }
        
        request.allHTTPHeaderFields = baseHeaders
        
        //setBody
        if let body = requestProvider.getBody(){
            let jsonData = try! JSONSerialization.data(withJSONObject: body, options: [])
            request.httpBody = jsonData
        }
        
        let task = session.dataTask(with: request) { data, response, error in
            
            if let error = error {
                requestProvider.notifyRequestFinished(with: error)
                return
            }
            
            guard let data = data else {
                requestProvider.notifyRequestFinished(with: NSError(domain:"No data returned error!", code:-1, userInfo:nil))
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
                if let responseData = json as? [String : Any]{
                    if let errorCode = responseData["errorCode"], let errorMessage = responseData["errorMessage"] as? String{
                        requestProvider.notifyRequestFinished(with: NSError(domain:errorMessage, code:Int(String(describing: errorCode)) ?? 0, userInfo:nil))
                    }else{
                        requestProvider.parseData(from: responseData)
                        requestProvider.notifyRequestFinishedSuccessfully()
                    }
                }else{
                    requestProvider.notifyRequestFinished(with: NSError(domain:"Parse error!", code:-1, userInfo:nil))
                }
            } catch {
                requestProvider.notifyRequestFinished(with: NSError(domain:"Serialization error!", code:-1, userInfo:nil))
            }
        }
        
        task.resume()
    }
}

extension MKNetworkManager{
    func mergeHeaders(dicts: [[String: String]]) -> [String : String]{
        var finalDict = [String : String]()

        for dict in dicts{
            for (k, v) in dict {
                finalDict.updateValue(v, forKey: k)
            }
        }

        return finalDict
    }
}
