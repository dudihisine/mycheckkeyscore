//
//  Protocols.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 26/05/2020.
//  Copyright © 2020 MyCheck. All rights reserved.
//

import UIKit

public protocol MyCheckKeysDelegate {
    func onInit()
    func onFailure(error : NSError)
    func onAvailableRoomsReceived(rooms : [MKRoom])
    func onDoorUnlocked()
    func onDoorUnlockedFailed(message : String)
    func onDeactivateAllKeys()
}

public protocol MyCheckKeys {
    func config(config : MKConfig, and delegate : MyCheckKeysDelegate)
    func activateRoom(room : MKRoom, maxActivationTimeInSec: TimeInterval)
    func activateRoom(room : MKRoom)
    func getAllAvailableRooms() ->[MKRoom]
    func deactivateAllKeys()
    func unregisterUser()
}

public protocol KeysProvider{
    func get()->MyCheckKeys
}
