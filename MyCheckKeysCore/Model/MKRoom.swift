//
//  MKRoom.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 09/11/2019.
//  Copyright © 2019 MyCheck. All rights reserved.
//

import Foundation

public class MKRoom{
    public var index : Int = 0
    public var reservationNumber : String?
    public var checkinDate : String?
    public var timeStampUTC : String?
    public var checkoutDateUTC : String?
    public var roomNumber : String?

    public init(){
        
    }

}
