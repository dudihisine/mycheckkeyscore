# MyCheckUtils
[![CI Status](http://img.shields.io/travis/eladsc/MyCheckUtils.svg?style=flat)](https://travis-ci.org/eladsc/MyCheckUtils)
[![Version](https://img.shields.io/cocoapods/v/MyCheckUtils.svg?style=flat)](http://cocoapods.org/pods/MyCheckUtils)
[![License](https://img.shields.io/cocoapods/l/MyCheckUtils.svg?style=flat)](http://cocoapods.org/pods/MyCheckUtils)
[![Platform](https://img.shields.io/cocoapods/p/MyCheckUtils.svg?style=flat)](http://cocoapods.org/pods/MyCheckUtils)

- source 'https://www.bitbucket.org/mycheck/mycheckutils_ios.git'
- This will set both the public CocoaPods Repo and the MyCheck private repo as targets for CocoaPods to search for frameworks from.

## install pod

    pod "MyCheckUtils"
    
## Remove Unused Architectures:
Our pod uses 'universal freamwork', in order to use the pod on devices and simulators you will need to add this 'Run script'.
Select the Project, Choose Target → Project Name → Select Build Phases → Press “+” → New Run Script Phase → Name the Script as “Remove Unused Architectures Script”.
    
    FRAMEWORK="MyCheckUtils"
    FRAMEWORK_EXECUTABLE_PATH="${BUILT_PRODUCTS_DIR}/${FRAMEWORKS_FOLDER_PATH}/$FRAMEWORK.framework/$FRAMEWORK"
    EXTRACTED_ARCHS=()
    for ARCH in $ARCHS
    do
    lipo -extract "$ARCH" "$FRAMEWORK_EXECUTABLE_PATH" -o "$FRAMEWORK_EXECUTABLE_PATH-$ARCH"
    EXTRACTED_ARCHS+=("$FRAMEWORK_EXECUTABLE_PATH-$ARCH")
    done
    lipo -o "$FRAMEWORK_EXECUTABLE_PATH-merged" -create "${EXTRACTED_ARCHS[@]}"
    rm "${EXTRACTED_ARCHS[@]}"
    rm "$FRAMEWORK_EXECUTABLE_PATH"
    mv "$FRAMEWORK_EXECUTABLE_PATH-merged" "$FRAMEWORK_EXECUTABLE_PATH"

## How to use

### Configure the sdk with Environment & Delegate

    MCUtils.sheard.configure(environment: .sandbox, debugMode: true)

### Call Login and pass PublishKey & RefreshToken

        MCUtils.sheard.login(publishKey: publishKey, refreshToken: refreshToken, completionBlock:{ accessToken,error in
            if let error = error{
                //Login request is finished with Error
                
            }
            
            if let accessToken = accessToken{
                Login request finished with accessToken & refreshToken
                //AccessToken is used for later requests to MyCheck servers
                self.print(message: "DID FINISH WITH DATA\n\naccessToken: \(accessToken)\n\nand refreshToken: \(refreshToken)")
            }
        })
        
### Call if After Getting expired token from server

refresh the token after expired token from server wont need publishKey & previews refreshToken because the SDK already has them

          MCUtils.sheard.handleTokenExpired(completionBlock: { accessToken,error in
            if let error = error{
                //RefreshToken request is finished with Error
                self.print(message:"DID FINISH WITH ERROR\n\n \(error.localizedDescription)")
            }
            
            if let accessToken = accessToken{
                //TokenExpired finished with accessToken & refreshToken
                //AccessToken is used for later requests to MyCheck servers
                self.print(message: "DID FINISH WITH DATA\n\naccessToken: \(accessToken)")
            }
        })
