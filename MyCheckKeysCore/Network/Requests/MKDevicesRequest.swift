//
//  MKDevicesRequest.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 17/06/2019.
//  Copyright © 2019 ErezSpatz. All rights reserved.
//

import UIKit

public protocol MKDevicesRequestDelegate {
    func didFinishRequestWithData(data : MKDevicesData?)
    func didFinishWithError(error : Error)
}

class MKDevicesRequest: MKBaseRequest {
    
    var delegate : MKDevicesRequestDelegate?
    var baseData : MKDevicesData?
    
    var body : [String : Any]?
    var header : [String : String]?
    
    //header - accessToken
    //body - deviceID
    
    init(deviceID : String, accessToken : String) {
        body = ["deviceId" : deviceID]
        header = ["authorization" : accessToken]
    }
    
    override func getModuleName() -> String {
        return "devices"
    }
    
    override func getBody() -> Dictionary<String, Any>? {
        return body
    }
    
    override func getHeader() -> Dictionary<String, String>? {
        return header
    }
    
    //externalDevice ID
    
    override func parseData(from response: [String : Any]) {
        let data = MKDevicesData()
        
        if let externalDeviceId = response["externalDeviceId"] as? String{
            data.externalDeviceId = externalDeviceId
        }
        
        if let token = response["token"] as? String{
            data.token = token
        }
        
        if let status = response["status"] as? String{
            data.status = status == "OK"
        }
        
        baseData = data
    }
    
    override func notifyRequestFinished(with error: Error) {
        if let delegate = delegate{
            delegate.didFinishWithError(error: error)
        }
    }
    
    override func notifyRequestFinishedSuccessfully() {
        if let delegate = delegate{
            delegate.didFinishRequestWithData(data: baseData)
        }
    }
}
