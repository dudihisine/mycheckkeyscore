//
//  MKConfig.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 09/11/2019.
//  Copyright © 2019 MyCheck. All rights reserved.
//

import Foundation
import MyCheckUtils

public class MKConfig : NSObject{
    var deviceID : String!
    
    var accessToken : String!
    
    var publishableKey : String!
    
    var refreshToken : String!

    var environment : MCUEnvironment!

    var debugMode : Bool!
    
    public init(environment : MCUEnvironment = .prod, publishableKey : String, refreshToken : String, accessToken : String, deviceID : String, debugMode : Bool = false) {
        super.init()
        self.deviceID = deviceID
        self.accessToken = accessToken
        self.publishableKey = publishableKey
        self.refreshToken = refreshToken
        self.environment = environment
        self.debugMode = debugMode
    }
    
    public func toString() ->String{
        return "config{ Eenvironment = '\(environment.toString())', DeviceId = '\(String(describing: deviceID))', PublishableKey = '\(String(describing: publishableKey))', RefreshToken = '\(String(describing: refreshToken))', AccessToken = '\(String(describing: accessToken))', DebugMode = '\(debugMode ? "Enabled" : "Disabled")'}"
    }
}

extension MCUEnvironment{
    func toString()->String{
        switch self {
        case .test:
            return "Test"
        case .sandbox:
            return "Sandbox"
        case .prod:
            return "Production"
        @unknown default:
            return "Unknown"
        }
    }
}
