//
//  MKSettings.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 17/06/2019.
//  Copyright © 2019 ErezSpatz. All rights reserved.
//

import UIKit

open class MKSettings{

    //the token that retrived after first regsiteration
    public var sessionToken : String?
    
    public var externalDeviceId : String?

    public init(){

    }

    public func set(sessionToken : String, externalDeviceId: String) {
        self.sessionToken = sessionToken
        self.externalDeviceId = externalDeviceId
    }
}
