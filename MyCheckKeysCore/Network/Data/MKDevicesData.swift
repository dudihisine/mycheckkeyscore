//
//  MKDevicesData.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 17/06/2019.
//  Copyright © 2019 ErezSpatz. All rights reserved.
//

import UIKit

public class MKDevicesData: NSObject {
    public var token : String?
    public var externalDeviceId : String?
    public var status : Bool = false
}
