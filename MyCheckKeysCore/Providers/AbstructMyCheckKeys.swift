//
//  AbstructMyCheckKeys.swift
//  MyCheckKeysCore
//
//  Created by Dudi Hisine on 17/06/2019.
//  Copyright © 2019 ErezSpatz. All rights reserved.
//

import UIKit
import MyCheckUtils
import CoreBluetooth

open class AbstructMyCheckKeys: NSObject, MyCheckKeys, MyCheckKeysDelegate{

    fileprivate var centralManager: CBCentralManager?
    fileprivate var peripherialManager: CBPeripheralManager?

    fileprivate var networkManager : MKNetworkManager?

    fileprivate var delegate : MyCheckKeysDelegate!

    open func config(config : MKConfig, and delegate : MyCheckKeysDelegate){

        //MARK: Check deviceID, accessToken, PublisjableKey, refreshToken not Empty
        if let error = checkEsentialParams(config: config){
            onFailure(error: error)
            return
        }

        //MARK: Place delegate
        self.delegate = delegate

        //MARK: Config Utils
        MCUtils.sheard.configure(environment: config.environment, debugMode: true)

        //MARK: Login
        config.validateLogin { [weak self] (error) in
            guard let self = self else { return }

            if let error = error as NSError?{
                self.onFailure(error: error)
                return
            }

            self.initSDK(config: config)
        }
    }

    open func initSDK(config: MKConfig) {

    }

    open func sendDevicesRequest(delegate: MKDevicesRequestDelegate){
        guard let config = getConfig() else{
            delegate.didFinishWithError(error: NSError(domain: "config isnt initialized", code: -1))
            return
        }

        if let networkManager = networkManager{
            let request = MKDevicesRequest(deviceID: config.deviceID, accessToken: config.accessToken)
            request.delegate = delegate
            networkManager.sendRequest(with: request)
            return
        }

        networkManager = MKNetworkManager(environment: config.environment)
        sendDevicesRequest(delegate: delegate)
    }

    fileprivate func checkEsentialParams(config : MKConfig) ->NSError?{
        //MARK: Check deviceID, accessToken, PublisjableKey, refreshToken not Empty
        if config.deviceID.isEmpty || config.accessToken.isEmpty || config.publishableKey.isEmpty || config.refreshToken.isEmpty{
            var error = "This variables are empty - "

            if config.deviceID.isEmpty {
                error += "deviceID, "
            }
            if config.accessToken.isEmpty {
                error += "accessToken, "
            }
            if config.publishableKey.isEmpty {
                error += "publishableKey, "
            }
            if config.refreshToken.isEmpty {
                error += "refreshToken, "
            }
            return NSError(domain: error, code: -1)
        }

        return nil
    }

    open func getConfig()->MKConfig?{
        return nil
    }

    open func activateRoom(room: MKRoom) {
        activateRoom(room: room, maxActivationTimeInSec: 30)
    }

    open func activateRoom(room: MKRoom, maxActivationTimeInSec: TimeInterval) {

    }

    open func getAllAvailableRooms() -> [MKRoom] {
        return []
    }

    open func deactivateAllKeys() {

    }

    open func unregisterUser() {

    }

    open func onInit() {
        if let delegate = delegate{
            delegate.onInit()
        }
    }

    open func onFailure(error: NSError) {
        if let delegate = delegate{
            delegate.onFailure(error: error)
        }
    }

    open func onAvailableRoomsReceived(rooms: [MKRoom]) {
        if let delegate = delegate{
            delegate.onAvailableRoomsReceived(rooms: rooms)
        }
    }

    open func onDoorUnlocked() {
        if let delegate = delegate{
            delegate.onDoorUnlocked()
        }
    }

    open func onDoorUnlockedFailed(message: String) {
        if let delegate = delegate{
            delegate.onDoorUnlockedFailed(message: message)
        }
    }

    open func onDeactivateAllKeys() {
        if let delegate = delegate{
            delegate.onDeactivateAllKeys()
        }
    }
}

extension AbstructMyCheckKeys{
    public func checkBTPermission() -> MKBTPermissionStatus {
        if #available(iOS 13.1, *) {
            return MKBTPermissionStatus(rawValue: CBCentralManager.authorization.rawValue) ?? .unknown
        }else if #available(iOS 13.0, *){
            return MKBTPermissionStatus(rawValue: CBCentralManager().authorization.rawValue) ?? .unknown
        }

        return MKBTPermissionStatus(rawValue: CBPeripheralManager.authorizationStatus().rawValue) ?? .unknown
    }

    public func askForBTPermission(){
        let showPermissionAlert = 1
        let options = [CBCentralManagerOptionShowPowerAlertKey: showPermissionAlert]

        if #available(iOS 13.0, *){
            centralManager = CBCentralManager(delegate: self, queue: nil, options: options)
        }

        peripherialManager = CBPeripheralManager(delegate: self, queue: nil, options: options)
    }

    public func showSettingsAlert(title:String,message:String,onCancel: @escaping (()->())){
        var rootViewController = UIApplication.shared.keyWindow?.rootViewController
        if let navigationController = rootViewController as? UINavigationController {
            rootViewController = navigationController.viewControllers.first
        }
        if let tabBarController = rootViewController as? UITabBarController {
            rootViewController = tabBarController.selectedViewController
        }

        guard let vc = rootViewController else{
            onCancel()
            return
        }

        AbstructMyCheckKeys.show2ButtonsAlert(onVC: vc, title: title, message: message, button1Title: "Settings", button2Title: "Cancel", onButton1Click: {
            if let settingsURL = URL(string: UIApplication.openSettingsURLString){
                UIApplication.shared.open(settingsURL, options: [:])
            }
        }) {
            onCancel()
        }
   }

    internal class func show2ButtonsAlert(onVC viewController:UIViewController, title:String, message:String, button1Title:String, button2Title:String, onButton1Click: @escaping (()->()), onButton2Click: @escaping (()->())){
        DispatchQueue.main.async() {
            let alert : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: button1Title, style:.default, handler: { (action:UIAlertAction) in
                       onButton1Click()
                   }))

            alert.addAction(UIAlertAction(title: button2Title, style:.default, handler: { (action:UIAlertAction) in
                       onButton2Click()
                   }))

            viewController.present(alert, animated: true, completion: nil)
        }
    }
}

extension AbstructMyCheckKeys: CBCentralManagerDelegate{
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {

    }
}

extension AbstructMyCheckKeys: CBPeripheralManagerDelegate{
    public func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {

    }
}

internal extension MKConfig{
    func validateLogin(completionHandler: @escaping (_ error : Error?) -> Void){
        if isExpired(){
            MCUtils.sheard.login(publishKey: publishableKey, refreshToken: refreshToken) { (token, error) in
                if let error = error{
                    completionHandler(error)
                    return
                }

                self.accessToken = token
                completionHandler(nil)
            }
        }else{
            completionHandler(nil)
        }
    }

    func isExpired() ->Bool{
        return MCUtils.sheard.isAccessTokenExpired(accessToken: accessToken)
    }
}
