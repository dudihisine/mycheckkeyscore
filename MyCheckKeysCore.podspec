Pod::Spec.new do |s|
  s.name             = 'MyCheckKeysCore'
  s.version          = '1.0.0'
  s.summary          = 'A SDK that enables the developer to open a Hotel room.'
  s.module_name      = 'MyCheckKeysCore'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://mycheckapp.com'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'eladsc' => 'eladsc@mycheck.co.il' }
  s.source           = { :git => 'https://www.bitbucket.org/dudihisine/mycheckkeyscore.git', :tag => s.version.to_s }
  s.swift_versions = '5.0'

  s.ios.deployment_target = '11.0'

  s.dependency 'MyCheckUtils'
  s.source_files = 'MyCheckKeysCore/Model/**/*','MyCheckKeysCore/Network/**/*','MyCheckKeysCore/Providers/**/*','MyCheckKeysCore/Utils/**/*'
  
end
